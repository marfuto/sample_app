User.find_or_create_by!( email: "example@railstutorial.org") do |user|
  user.name =  "Example User"
  user.password = "foobar"
  user.password_confirmation = "foobar"
  user.admin = true
  user.activated = true
  user.activated_at = Time.zone.now
end

99.times do |n|
  mail = "example-#{n+1}@railstutorial.org"
  User.find_or_create_by(email: mail) do |user|
    user.name = Faker::Name.name
    user.password = "password"
    user.password_confirmation = "password"
    user.activated = true
    user.activated_at = Time.zone.now
  end
end

users = User.order(:created_at).take(6).select {|u| u.microposts.count < 50}
50.times do
  content = Faker::Lorem.sentence(5)
  users.each { |user| user.microposts.create!(content: content) }
end

# Взаимоотношения
users = User.all
user  = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) if !user.following.include?(followed) }
followers.each { |follower| follower.follow(user) if !follower.following.include?(user)}
